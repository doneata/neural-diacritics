import argparse
import os
import pdb

import numpy as np

from keras.callbacks import (
    ModelCheckpoint,
)

from keras.layers import (
    Bidirectional,
    Dense,
    Dropout,
    Embedding,
    Input,
    LSTM,
)

from keras.models import (
    Model,
    load_model,
)

from keras.utils import (
    to_categorical,
)


DATA_PATH = 'data'


CHARS_INP = ' \n-0123456789abcdefghijklmnopqrstuvwxyz'
CHARS_OUT = ' \n-0123456789abcdefghijklmnopqrstuvwxyzăâîşţ'

NO_DIACRITICS_MAP = {
    'ă': 'a',
    'â': 'a',
    'î': 'i',
    'ş': 's',
    'ţ': 't'
}


def read_data(name):
    path = os.path.join(DATA_PATH, name)
    with open(path, 'r', encoding='utf-8') as f:
        return f.read()


def write_data(name, text):
    path = os.path.join(DATA_PATH, name)
    with open(path, 'w', encoding='utf-8') as f:
        return f.write(text)


def remove_diacritics(text):
    return text.translate(str.maketrans(NO_DIACRITICS_MAP))


def encode(text, chars):
    char_to_id = {c: i for i, c in enumerate(chars)}
    return [char_to_id[c] for c in text]


def decode(idxs, chars):
    id_to_char = dict(enumerate(chars))
    return ''.join(id_to_char[i] for i in idxs)


def group_in_chunks(xs, k, pad=False):
    m = len(xs) // k
    xs = np.array(xs)
    if pad:
        xs = np.hstack((xs, np.zeros(k * (m + 1) - len(xs))))
    else:
        xs = xs[:m * k]
    return xs.reshape(-1, k)


def group_in_overlapping_chunks(xs, k):
    pass


def get_model(seq_len=16, emb_dim=64, lstm_dim=64):
    inp = Input(shape=(None, ))
    emb = Embedding(input_dim=len(CHARS_INP), output_dim=emb_dim)(inp)
    lstm = LSTM(
        lstm_dim,
        return_sequences=True,
        input_shape=(None, 32),
        dropout=0.2,
        recurrent_dropout=0.2,
    )
    bilstm = Bidirectional(lstm)(emb)
    out = Dropout(0.2)(Dense(len(CHARS_OUT), activation='softmax')(bilstm))
    model = Model(inp, out)
    return model


def train():
    seq_len = 128
    emb_dim = 64
    lstm_dim = 128

    text_out = read_data('talkshows.train_v7')
    # Subsample training data.
    text_out = '\n'.join(text_out.split('\n')[::17])
    text_inp = remove_diacritics(text_out)

    if True:
        model = get_model(seq_len, emb_dim, lstm_dim)
        model.compile(loss='categorical_crossentropy', optimizer='adam')
        print(model.summary())
    else:
        # Continue training
        model = load_model('models/simple_bilstm_v01_best.h5')

    checkpoint = ModelCheckpoint(
        'models/simple_bilstm_v01_best.h5',
        verbose=1,
        save_best_only=True,
    )

    X = group_in_chunks(encode(text_inp, CHARS_INP), seq_len)
    Y = group_in_chunks(encode(text_out, CHARS_OUT), seq_len)
    Y = to_categorical(Y, num_classes=len(CHARS_OUT))

    model.fit(
        X, Y,
        batch_size=128,
        epochs=64,
        validation_split=0.01,
        callbacks=[checkpoint],
    )


def predict():
    seq_len = 128
    model = load_model('models/simple_bilstm_v01_best.h5')
    text_inp = read_data('talkshows.dev_v7.wodia')
    X = group_in_chunks(encode(text_inp, CHARS_INP), seq_len)
    # TODO Improve predicition – either stateful model or variable length input
    text_out = decode(
        model.predict(X, verbose=1).argmax(axis=2).flatten(),
        CHARS_OUT)
    write_data('dev.restored', text_out)


def main():
    parser = argparse.ArgumentParser(
        description='Deep learning for diacritics restoration.')

    parser.add_argument(
        '--todo',
        choices=['train', 'predict'],
        nargs='+',
        help='what to do',
    )

    args = parser.parse_args()

    if 'train' in args.todo:
        train()

    if 'predict' in args.todo:
        predict()


if __name__ == '__main__':
    main()
