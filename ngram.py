# TODO
# - [ ] Do not add bigrams for words at boundaries (begin / end of sentence)
# - [ ] Use all training sources

import argparse
import math
import pdb
import pickle
import sys

from collections import (
    Counter,
    defaultdict,
)

from functools import lru_cache

from main import (
    read_data,
    remove_diacritics,
)


sys.setrecursionlimit(2500)


def word_accuracy(true_words, pred_words):
    return sum(t == p for t, p in zip(true_words, pred_words)) / len(true_words)


def load_params():
    with open('models/ngram.pkl', 'rb') as f:
        params = pickle.load(f)
        map_ = pickle.load(f)
    return params, map_


def map_no_diacritics_to_diacritics(words):
    words_ = remove_diacritics(' '.join(words)).split() 
    m = defaultdict(list)
    for w_, w in set(zip(words_, words)):
        m[w_].append(w)
    return m


def get_choice(m, word):
    if word in m:
        return m[word]
    else:
        return [word]


eps = math.log(1e-8)


def best_path(choices, params, alpha=1.0):

    unary, binary = params

    @lru_cache(maxsize=1024)
    def score(i, w, path):
        if i == 0:
            return unary.get(w, eps), (w, )
        curr_choices = choices[i - 1]
        scores_paths = (score(i - 1, v, path) for v in curr_choices)
        scores_paths = (
            (s + alpha * binary.get((v, w), eps) + unary.get(w, eps), p + (w, ))
            for (s, p), v in zip(scores_paths, curr_choices))
        return max(scores_paths, key=lambda t: t[0])

    n = len(choices)
    best_score, best_path = max(score(n - 1, w, ()) for w in choices[-1])
    return best_path


def train():
    sets = ['talkshows.train_v7', 'libertatea_wdia_v7', 'realitatea_wdia_v7']
    texts = (read_data(s) for s in sets)
    words = ' '.join(texts).split()

    unary = Counter(words)
    total = sum(unary.values())
    unary_probs = {w: math.log(c / total) for w, c in unary.items()}

    binary = Counter(zip(words, words[1:]))
    binary_probs = {
        (u, v): math.log(c / unary[u])
        for (u, v), c in binary.items()}

    p = unary_probs, binary_probs
    m = map_no_diacritics_to_diacritics(words)

    with open('models/ngram.pkl', 'wb') as f:
        pickle.dump(p, f)
        pickle.dump(m, f)

    return p, m


def predict_sentence(line, params, map_, alpha=1.0):
    words = line.split()
    choices = [get_choice(map_, word) for word in words]
    preds = best_path(choices, params, alpha)
    return ' '.join(preds)


def evaluate(true_text, params, map_, alpha):
    text = remove_diacritics(true_text)
    lines = text.strip().split('\n')
    pred = (predict_sentence(line, params, map_, alpha) for line in lines)
    pred_text = ' '.join(pred)
    return word_accuracy(true_text.split(), pred_text.split())


def predict(test_data, params, map_):
    text = read_data('talkshows.{}_v7.wodia'.format(test_data))
    lines = text.strip().split('\n')
    with open('data/{}.restored'.format(test_data), 'w', encoding='utf-8') as f:
        for i, line in enumerate(lines):
            pred = predict_sentence(line, params, map_, alpha=3.0)
            f.write(pred)
            f.write('\n')
            if i % 1000 == 0:
                sys.stdout.write('{:10d}\r'.format(i))
                sys.stdout.flush()


def main():
    parser = argparse.ArgumentParser(
        description='N-gram model for diacritics restoration.')

    parser.add_argument(
        '--todo',
        choices=['train', 'evaluate', 'predict'],
        nargs='+',
        help='what to do',
    )

    parser.add_argument(
        '--test_data',
        choices=['dev', 'test'],
        default='dev',
        help='which data to predict on',
    )

    args = parser.parse_args()

    if 'train' in args.todo:
        params, map_ = train()

    if 'evaluate' in args.todo:
        text = read_data('talkshows.train_v7')
        text = '\n'.join(text.split('\n')[::50])
        params, map_ = load_params()
        for alpha in [0.1, 0.3, 1.0, 3.0, 5.0, 10.0]:
            score = evaluate(text, params, map_, alpha)
            print('{:6.2f} {:.2f}'.format(alpha, 100 * score))

    if 'predict' in args.todo:
        params, map_ = load_params()
        predict(args.test_data, params, map_)


if __name__ == '__main__':
    main()
