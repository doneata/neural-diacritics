A simple architecture for diacritics restoration.

Install requirements:

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

Train:

```bash
python main.py --todo train
```

Predict:

```bash
python main.py --todo predict
```
